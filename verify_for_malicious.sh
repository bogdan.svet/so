#!/bin/bash

file_path="$1"
dir_path="$2"

chmod 444 "$file_path"

lines=$(wc -l < "$file_path")
words=$(wc -w < "$file_path")
chars=$(wc -m < "$file_path")

if grep -q -P -n "[^\x00-\x7F]" "$file_path" || grep -q -E "corrupted|dangerous|risk|attack|malware|malicious" "$file_path"; then
    chmod 000 "$file_path"
    # echo "!!! NOT SAFE $file_path"
    mv "$file_path" "$dir_path"
elif [ $lines -lt 3 ] && [ $words -gt 1000 ] && [ $chars -gt 2000 ]; then
    chmod 000 "$file_path"
    # echo "!!! NOT SAFE $file_path"
    mv "$file_path" "$dir_path"
else
    chmod 000 "$file_path"
    # echo "!!! SAFE $file_path"
fi
