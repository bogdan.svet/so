#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

// Caz de executie:     ./exec -s DIR_IZOLATOR DIRECTOR... FISIER...
// Caz de executie:     ./exec -o DIR_OUTPUT -s DIR_IZOLATOR DIRECTOR... FISIER...

// Pentru afisare SAFE / NOT SAFE -> Fisier verify_for_malicious.sh     Liniile: 14, 18, 22
// Pentru afisare modificari noi fiecare Snapshot                       Linia: 405

// Variabila globala DIR_IZOLATOR
char izolatedDIRPath[300];

// Functie prelucrare Director / Fisier
void path_stat(const char *path, int *snapFile, int *pfd)
{
    struct stat st;
    stat(path, &st);

    char info[1000];

    // Conditie Director / Fisier
    if (S_ISDIR(st.st_mode))
        sprintf(info, "Intrare: %s - DIRECTOR\n", path);
    else
        sprintf(info, "Intrare: %s - FISIER\n", path);

    // Dimensiune Director / Fisier
    char sizeInfo[200];
    sprintf(sizeInfo, "Dimensiune: %ld octeti\n", st.st_size);
    strcat(info, sizeInfo);

    // Ultima modificare Director / Fisier
    char mtimeInfo[200];
    char mtimeFormat[20];
    sprintf(mtimeInfo, "Ultima modificare: ");
    strftime(mtimeFormat, 20, "%Y-%m-%d %H:%M:%S", localtime(&st.st_mtime));
    strcat(mtimeInfo, mtimeFormat);

    strcat(info, mtimeInfo);
    strcat(info, "\n");

    // Variabila pentru a verifica daca suma permisiuni va fi 0
    int sumaPermisiuni = 0;

    char modeInfo[200];
    sprintf(modeInfo, "Permisiuni: ");

    // Read User
    if (S_IRUSR & st.st_mode)
    {
        strcat(modeInfo, "r");
        sumaPermisiuni++;
    }
    else
        strcat(modeInfo, "-");

    // Write User
    if (S_IWUSR & st.st_mode)
    {
        strcat(modeInfo, "w");
        sumaPermisiuni++;
    }
    else
        strcat(modeInfo, "-");

    // Execute User
    if (S_IXUSR & st.st_mode)
    {
        strcat(modeInfo, "x");
        sumaPermisiuni++;
    }
    else
        strcat(modeInfo, "-");

    // Read Group
    if (S_IRGRP & st.st_mode)
    {
        strcat(modeInfo, "r");
        sumaPermisiuni++;
    }
    else
        strcat(modeInfo, "-");

    // Write Group
    if (S_IWGRP & st.st_mode)
    {
        strcat(modeInfo, "w");
        sumaPermisiuni++;
    }
    else
        strcat(modeInfo, "-");

    // Execute Group
    if (S_IXGRP & st.st_mode)
    {
        strcat(modeInfo, "x");
        sumaPermisiuni++;
    }
    else
        strcat(modeInfo, "-");

    // Read Others
    if (S_IROTH & st.st_mode)
    {
        strcat(modeInfo, "r");
        sumaPermisiuni++;
    }
    else
        strcat(modeInfo, "-");

    // Write Others
    if (S_IWOTH & st.st_mode)
    {
        strcat(modeInfo, "w");
        sumaPermisiuni++;
    }
    else
        strcat(modeInfo, "-");

    // Execute Others
    if (S_IXOTH & st.st_mode)
    {
        strcat(modeInfo, "x");
        sumaPermisiuni++;
    }
    else
        strcat(modeInfo, "-");

    strcat(info, modeInfo);
    strcat(info, "\n");

    // Numar Inode Director / Fisier
    char inodeInfo[200];
    sprintf(inodeInfo, "Numar Inode: %ld\n", st.st_ino);

    strcat(info, inodeInfo);
    strcat(info, "\n");

    // Daca suma Permisiuni nu este 0 atunci inscriem informatiile despre Director / Fisier in Snapshot
    // Altfel verificam daca este SAFE / NOT SAFE
    if (sumaPermisiuni != 0)
        write(*snapFile, info, strlen(info));
    else
    {
        int pid;

        if ((pid = fork()) < 0)
        {
            perror("Eroare fork");
            exit(1);
        }

        if (pid == 0)
        {
            // Citim din Pipe cate Directoare / Fisiere Suspecte avem
            int primit = 0;
            read(pfd[0], &primit, sizeof(int));

            // Inscriem in Pipe inca un Director / Fisier Suspect
            int peric = primit + 1;
            write(pfd[1], &peric, sizeof(int));
            close(pfd[1]);

            // Executam fisierul de verificare SAFE / NOT SAFE
            execlp("/bin/sh", "bash", "verify_for_malicious.sh", path, izolatedDIRPath, "&", NULL);

            exit(0);
        }

        wait(NULL);
    }
}

// Functie verificare daca avem deja Snapshot pentru un Director
// Return 1     Snapshot exista, redenumim ca Old
// Return 0     Nu exista Snapshot
// Return -1    Nu e un Director
int hasSnapshot(const char *path)
{
    struct stat st;
    stat(path, &st);

    if (S_ISDIR(st.st_mode))
    {
        struct dirent *db;
        DIR *director = opendir(path);

        while ((db = readdir(director)) != NULL)
        {
            char newPath[300];
            sprintf(newPath, "%s/%s", path, db->d_name);

            if (!strcmp(db->d_name, "Snapshot.txt"))
            {
                char oldPath[300];
                sprintf(oldPath, "%s/%s", path, "Snapshot.txt");

                char newPath[300];
                sprintf(newPath, "%s/%s", path, "SnapshotOld.txt");

                rename(oldPath, newPath);

                return 1;
            }
        }

        closedir(director);
    }
    else
    {
        return -1;
    }

    return 0;
}

// Functie parcurgere recursiva fiecare Director
void parcurgere(const char *path, int isRoot, int hasSnap, int *snapFile, int *pfd)
{
    // Daca suntem in Directorul Radacina si acesta nu are un Fisier Snapshot din Director Output
    if (isRoot && *snapFile == -1)
    {
        // Daca Suntem in Director
        if (hasSnap != -1)
        {
            char snapshotPath[300];
            sprintf(snapshotPath, "%s/Snapshot.txt", path);

            // O_CREAT      Creare Fisier Snapshot daca nu exista
            // O_WRONLY     Doar Scriere
            // O_TRUNC      Scriere de la 0
            // S_IRUSR      Permisiune Read User
            // S_IWUSR      Permisiune Write user
            int snapFile = open(snapshotPath, O_CREAT | O_WRONLY | O_TRUNC, S_IRUSR | S_IWUSR);

            if (snapFile == -1)
            {
                perror("Error creating Snapshot.txt");
                exit(EXIT_FAILURE);
            }

            struct stat st;
            stat(path, &st);

            // Cand a fost creat Fisierul Snapshot
            char mtimeInfo[200];
            char mtimeFormat[20];
            sprintf(mtimeInfo, "Marca temporala: ");
            strftime(mtimeFormat, 20, "%Y-%m-%d %H:%M:%S", localtime(&st.st_mtime));
            strcat(mtimeInfo, mtimeFormat);
            strcat(mtimeInfo, "\n\n");

            write(snapFile, mtimeInfo, strlen(mtimeInfo));

            // Daca suntem iar in Director
            if (S_ISDIR(st.st_mode))
            {
                struct dirent *db;
                DIR *director = opendir(path);

                // Loop parcurgere fiecare cale din Director
                while ((db = readdir(director)) != NULL)
                {
                    char newPath[300];
                    sprintf(newPath, "%s/%s", path, db->d_name);

                    // Parcurgem daca calea nu e . / .. / Snapshot.txt / SnapshotOld.txt
                    if (strcmp(db->d_name, ".") && strcmp(db->d_name, "..") && strcmp(db->d_name, "Snapshot.txt") && strcmp(db->d_name, "SnapshotOld.txt"))
                        parcurgere(newPath, 0, hasSnapshot(newPath), &snapFile, pfd);
                }

                closedir(director);
            }

            close(snapFile);
        }
    }
    else
    {
        // Daca suntem in Director / Fisier fara Snapshot
        if (hasSnap != 1)
        {
            struct stat st;
            stat(path, &st);

            // Daca suntem iar in Director
            if (S_ISDIR(st.st_mode))
            {
                struct dirent *db;
                DIR *director = opendir(path);

                // Loop parcurgere fiecare cale din Director
                while ((db = readdir(director)) != NULL)
                {
                    char newPath[300];
                    sprintf(newPath, "%s/%s", path, db->d_name);

                    // Parcurgem daca calea nu e . / .. / Snapshot.txt / SnapshotOld.txt
                    if (strcmp(db->d_name, ".") && strcmp(db->d_name, "..") && strcmp(db->d_name, "Snapshot.txt") && strcmp(db->d_name, "SnapshotOld.txt"))
                        parcurgere(newPath, 0, hasSnapshot(newPath), snapFile, pfd);
                }

                closedir(director);
            }

            // Prelucram Director / Fisier
            path_stat(path, snapFile, pfd);
        }
        // Daca avem Snapshot dar nu Snapshot din Director Output
        if (hasSnap == 1 && *snapFile != -1)
        {
            struct stat st;
            stat(path, &st);

            // Daca suntem iar in Director
            if (S_ISDIR(st.st_mode))
            {
                struct dirent *db;
                DIR *director = opendir(path);

                // Loop parcurgere fiecare cale din Director
                while ((db = readdir(director)) != NULL)
                {
                    char newPath[300];
                    sprintf(newPath, "%s/%s", path, db->d_name);

                    // Parcurgem daca calea nu e . / .. / Snapshot.txt / SnapshotOld.txt
                    if (strcmp(db->d_name, ".") && strcmp(db->d_name, "..") && strcmp(db->d_name, "Snapshot.txt") && strcmp(db->d_name, "SnapshotOld.txt"))
                        parcurgere(newPath, 0, 1, snapFile, pfd);
                }

                closedir(director);
            }

            // Prelucram Director / Fisier
            path_stat(path, snapFile, pfd);
        }
    }
}

// Citim fiecare parte de cale din Snapshot
char *readPathPart(int *snapFile)
{
    ssize_t bytes;

    char snapBuffer[1000];
    memset(snapBuffer, 0, sizeof(snapBuffer));

    bytes = read(*snapFile, snapBuffer, sizeof(snapBuffer));

    if (bytes > 0)
    {
        for (int i = 0; i < bytes - 1; i++)
        {
            if (snapBuffer[i] == '\n' && snapBuffer[i + 1] == '\n')
            {
                snapBuffer[i] = '\0';

                char *result = malloc(i);

                strcpy(result, snapBuffer);

                lseek(*snapFile, -(bytes - (i + 2)), SEEK_CUR);

                return result;
            }
        }
    }

    return NULL;
}

// Comparam Snapshot nou cu cel vechi SnapshotOld
void compareSnapshots(const char *path)
{
    char snapPathNew[300];
    sprintf(snapPathNew, "%s/%s", path, "Snapshot.txt");

    char snapPathOld[300];
    sprintf(snapPathOld, "%s/%s", path, "SnapshotOld.txt");

    int snapNew = open(snapPathNew, O_RDONLY);
    int snapOld = open(snapPathOld, O_RDONLY);

    // Citim primele linii ca sa dam skip despre cand au fost create Snapshoturile
    char primaLinieNew[38];
    read(snapNew, primaLinieNew, sizeof(primaLinieNew));
    char primaLinieOld[38];
    read(snapOld, primaLinieOld, sizeof(primaLinieOld));

    char *bufferNew;
    char *bufferOld;

    // Citim si comparam fiecare parte de cale din ambele Snapshot-uri
    while ((bufferNew = readPathPart(&snapNew)) != NULL && (bufferOld = readPathPart(&snapOld)) != NULL)
    {
        if (strcmp(bufferNew, bufferOld))
            // printf("\n+++++++++++\nNEW VERSION\n%s\n-----------\nOLD VERSION\n%s\n\n", bufferNew, bufferOld);

            free(bufferNew);
        free(bufferOld);
    }
}

// Stergem Snapshot vechi
void deleteOldSnapshot(const char *path)
{
    char snapPath[300];
    sprintf(snapPath, "%s/%s", path, "SnapshotOld.txt");

    remove(snapPath);
}

int main(int argc, char *argv[])
{
    // Daca nu avem destule argumente
    if (argc == 1)
    {
        perror("NOT ENOUGH ARGUMENTS");
        exit(EXIT_FAILURE);
    }

    char *outputPath = NULL;
    int izolatedArgv = 0;

    // Aflam daca avem Director de Output si de Izolare
    for (int i = 0; i < argc - 1; i++)
    {
        if (strcmp(argv[i], "-o") == 0)
        {
            outputPath = argv[i + 1];
        }
        if (strcmp(argv[i], "-s") == 0)
        {
            izolatedArgv = i + 1;
            strcpy(izolatedDIRPath, argv[izolatedArgv]);
            mkdir(izolatedDIRPath, 0777);
        }
    }

    // Daca nu avem Director de izolare
    if (izolatedArgv == 0)
    {
        perror("No izolated path provided");
        exit(EXIT_FAILURE);
    }

    int pids = 0;

    // Daca nu avem Director de Output
    // Altfel daca avem Director de Output
    if (outputPath == NULL)
    {
        for (int i = izolatedArgv + 1; i < argc; i++)
        {
            int snapFile = -1;
            int hasSnap = hasSnapshot(argv[i]);

            if (hasSnap > -1)
            {
                int pfd[2];
                int pid;

                if (pipe(pfd) < 0)
                {
                    perror("Eroare pipe\n");
                    exit(1);
                }

                if ((pid = fork()) < 0)
                {
                    perror("Eroare fork");
                    exit(1);
                }

                pids++;

                if (pid == 0)
                {
                    int peric = 0;
                    write(pfd[1], &peric, sizeof(int));

                    parcurgere(argv[i], 1, hasSnap, &snapFile, pfd);

                    int primit = 0;
                    read(pfd[0], &primit, sizeof(int));
                    close(pfd[0]);

                    printf("Snapshot for Directory %s created successfully.\n", argv[i]);

                    if (hasSnap == 1)
                    {
                        compareSnapshots(argv[i]);
                        deleteOldSnapshot(argv[i]);
                    }

                    printf("Procesul Copil %d s-a incheiat cu PID-ul %d si cu %d fisiere cu potential periculos.\n", pids, getpid(), primit);

                    exit(0);
                }

                wait(NULL);
            }
        }
    }
    else
    {
        for (int i = izolatedArgv + 1; i < argc; i++)
        {
            if (hasSnapshot(argv[i]) > -1)
            {
                int pfd[2];
                int pid;

                if (pipe(pfd) < 0)
                {
                    perror("Eroare pipe\n");
                    exit(1);
                }

                if ((pid = fork()) < 0)
                {
                    perror("Eroare fork");
                    exit(1);
                }

                pids++;

                if (pid == 0)
                {
                    char snapshotDIR[300];
                    char snapshotPath[300];
                    sprintf(snapshotDIR, "%s/%s", outputPath, argv[i]);
                    sprintf(snapshotPath, "%s/%s/Snapshot.txt", outputPath, argv[i]);

                    mkdir(snapshotDIR, 0777);

                    hasSnapshot(snapshotDIR);

                    int snapFile = open(snapshotPath, O_CREAT | O_WRONLY | O_TRUNC, S_IRUSR | S_IWUSR);

                    if (snapFile == -1)
                    {
                        perror("Error creating Snapshot.txt");
                        exit(EXIT_FAILURE);
                    }

                    struct stat st;
                    stat(outputPath, &st);

                    char mtimeInfo[200];
                    char mtimeFormat[20];
                    sprintf(mtimeInfo, "Marca temporala: ");
                    strftime(mtimeFormat, 20, "%Y-%m-%d %H:%M:%S", localtime(&st.st_mtime));
                    strcat(mtimeInfo, mtimeFormat);
                    strcat(mtimeInfo, "\n\n");

                    write(snapFile, mtimeInfo, strlen(mtimeInfo));

                    int peric = 0;
                    write(pfd[1], &peric, sizeof(int));

                    parcurgere(argv[i], 1, 1, &snapFile, pfd);

                    int primit = 0;
                    read(pfd[0], &primit, sizeof(int));
                    close(pfd[0]);

                    printf("Snapshot for Directory %s created successfully.\n", argv[i]);

                    deleteOldSnapshot(argv[i]);

                    compareSnapshots(snapshotDIR);
                    deleteOldSnapshot(snapshotDIR);

                    close(snapFile);

                    printf("Procesul Copil %d s-a incheiat cu PID-ul %d si cu %d fisiere cu potential periculos.\n", pids, getpid(), primit);

                    exit(0);
                }

                wait(NULL);
            }
        }
    }

    exit(EXIT_SUCCESS);
}
